

import ARKit

protocol ItemListDragProtocol: class {
    func closeItemList()
    func showDirection(of object: ARItem)
    var currentTrackableItem: ARItem? { get set }
}

struct ARItem: Equatable {
    let name: String
    let anchor: ARAnchor
}

class ItemModel {
    static let shared = ItemModel()
    private var itemsList: [ARItem] = []
    
    public func getList() -> [ARItem] {
        return itemsList
    }
    
    public func addItem(name: String, anchor: ARAnchor) {
        let item = ARItem(name: name, anchor: anchor)
        itemsList.append(item)
    }
    
    public func removeAll() {
        itemsList.removeAll()
    }
    
}
